# Projet Web Service Maxime Boulogne

## Détail des Projet eclipse

Le projet évolue autour d'une base de donnée SQLite (dont le détail est visible dans le PDF), stocké à la racine du projetREST avec le nom "Train_bd1.bd" 

- TrainServiceREST
	Service déployé sur le port 8182
	Service Rest réalisé avec reslet. il permet de filtrer les trains de 3 manière : - Selon les stations de dépars et d'arrivé
											 - les stations + les dates de dépars et arrivé
											 - les stations + les dates + les nombre de siège dispo pour une catégorie donnée
	De même le REST permet de reserver une ou plusieurs place d'un trian en fournissant : l'id du train/ du client, le nombre de place, la classe des siège 
	choisies (business..) ainsi que la flexibilité ou non des billets.
	Cette reservation est en méthode @POST, met à jour avec une nouvelle ligne dans la table reservation (avec le dernier indentifiant de réservation non utilisé),
	et met à jour le nombre de places disponible du train une fois la réservation faite. Renvoie "True" et met à jour si les places sont libre, renvoie "False" sans
	mise à jour de la bd dans le cas contraire.
	Le REST possède l'accés à la base et ainsi récupère / modifie les infos de cette derniere suivant les requètes. 
	
	Exemple d'Url de test :
		filtre 1 : http://localhost:8182/searching/LILLE/MARSEILLE
		filtre 2 : http://localhost:8182/searching/LILLE/MARSEILLE/2024-01-02%2008:00:00.0/2024-01-06%2008:00:00.0
		filtre 3 : http://localhost:8182/searching/LILLE/MARSEILLE/2024-01-02%2008:00:00.0/2024-01-06%2008:00:00.0/45/first
			
		Reservation : http://localhost:8182/reserve Dans Postman (Body -> x.www-form-urlencoded : ("trainId", "19") ("nbTickets", "8") ("travelClass", "business") ("flexibility", "flexible") ("userID", "11")

- TrainServiceSOAP
	Service déployé avec Axis 2 sur le port 8080 à l'aide du serveur tomcat
	La classe qui y est contenu permet de lancer les requetes @GET et POST vers le service REST, suivant les valeur fournie par le client (ou une demande @GET dans POSTMAN)
	
	Exemple d'Url POSTMAN pour le SOAP: http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter2?departureStation=NICE&arrivalStation=MARSEILLE&departureDate=2024-01-01&arrivalDate=2024-01-04
	Pour une reservation, l'appel au service devient un @GET dans POSTMAN car la requete @POST vers le REST est définie à l'interieur de la méthode du SOAP, exemple :
	http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/reservePlaces?idTrain=8&nbPlaces=3&travelClass=standard&flexibilite=flexible&userId=14

- TrainServiceClient
	Le client est une classe java faisant appel aux méthode du SOAP, en développant des url, afin de pouvoir appeler les méthode à travers des @GET.
	Le resultat est affiché à tavers un main dans la console; ce resultat est au préalable caster afin d'enlever les balise qui apparaisse du au format "xml" des return du service SOAP


## Information sur l'utilisation du projet

Il suffit d'ouvrir "eclipse java EE IDE" avec pour workspace le dossier "Projet_eclipse".

Erreur à la première utilidation possible :
	- Dans windows -> préferences -> Web service -> Axis2 Préference --> mettre le dossier Sources/axis2-1.6.2 (si ce n'est pas le cas)
	- Créer un nouveau serveur avec Sources/Tomcat, dans le cas ou la réference à "Tomcat" du serveur existant est erronée
	- Si un fichier java est en erreur dans le REST, re apporter le Sources/org.restlet.jar dans le classpass du projet (clic droit projet->properties->JavaBuildPass->add external jar (avec le org.restlet.jar en enlever l'ancien)

Etape de lancement du projet :
	1. lancement de la classe "RESTDistributor" dans le REST permettant de mettre en marche le service
	2. clic droit sur la classe "passerelleSoap.java"->Web Services->Create web service->select axis2 dans "web service runtime"-> finish
	3. Et enfin clic droit sur le projet du service SOAP->Run As->Run on Serveur-> Select Tomcat Serveur

-> Et voila le Proket est opérationnel, près à recevoir des requete POSTMAN sur le service SOAP / REST et nous pouvons suivre les évoluions de la base de données
à travers l'outil "dbvisualiseur" (executable dans outil_db_visualiser/DB Browser for SQLite.exe, puis en ouvrant "Train_bd1.bd" contenu dans le projet REST).
De mème le client peut être executé et modifié afin d'utiliser le projet dans son ensemble.

## Auto-évaluation sur le travail réalisé

1.	Create REST Train Filtering service B -------------------------------------------- 5/6
2.	Create SOAP Train Booking service A ---------------------------------------------- 3/4
3.	Interaction between two services ------------------------------------------------- 3/4
4.	Test with Web service Client (instead of using Eclipse's Web service Explorer) --- 0.5/2
5.	Work with complex data type (class, table, etc.) --------------------------------- 1.5/2
6.	Work with database (in text file, xml, in mysql, etc.) --------------------------- 2/2
7.	Postman -------------------------------------------------------------------------- 2/2
8.	OpenAPI	-------------------------------------------------------------------------- 0/3
9.	BPMS ----------------------------------------------------------------------------- 1/5