package soap.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

//Classe permettant de faire la passerelle entre les requete du client et le traintement du REST communiquant avec la base
public class PaserelleSOAP {

	/** Fonction qui va lancer une requ�te @GET vers la ressource du reste permettant de filtrer les trains par stations, avec celles fournies par le client
	 * 
	 * @param departureStation Station de d�part s�lectionn�e par le client
	 * @param arrivalStation Station d'arriv� s�lectionn�e par le client
	 * @return Renvoie le r�sultat g�n�rer par le REST suivant les param�tre forunis
	 * @throws IOException
	 */
    public static String researchTrainsFilter1(String departureStation, String arrivalStation) throws IOException { // test axis : http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter1?departureStation=NICE&arrivalStation=PARIS
        String urlString = "http://localhost:8182/searching/" + departureStation + "/" + arrivalStation;
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            return response.toString();
        } else {
            return "GET request not worked, Response Code: " + responseCode;
        }
    }

    /** Fonction qui va lancer une requ�te @GET vers la ressource du reste permettant de filtrer les trains par stations et dates avec les info choisies par le client
     * 
     * @param departureStation station de d�part
     * @param arrivalStation station d'arriv�
     * @param departureDate date de d�part du train
     * @param arrivalDate date d'arriv� du train
     * @return Resultat de la recherche dans la base, �tablie par le service REST
     * @throws IOException
     */
    public static String researchTrainsFilter2(String departureStation, String arrivalStation, String departureDate, String arrivalDate) throws IOException { // test axis : http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter2?departureStation=NICE&arrivalStation=MARSEILLE&departureDate=2024-01-01&arrivalDate=2024-01-04
        String urlString = "http://localhost:8182/searching/" + departureStation + "/" +  arrivalStation + "/" + departureDate + "/" + arrivalDate;
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            return response.toString();
        } else {
            return "GET request not worked, Response Code: " + responseCode;
        }
    }

    /** Fonction qui va lancer une requ�te @GET vers la ressource du reste permettant de filtrer les trains par stations, dates et places disponibles avec les info choisies par le client
     * 
     * @param departureStation station de d�part
     * @param arrivalStation station d'arriv�
     * @param departureDate date de d�part du train
     * @param arrivalDate date d'arriv� du train
     * @param nbPlaces nombre de si�ges disponible au minimum
     * @param travelClass cat�gorie de si�ge dont on veut verifier le nombre de places
     * @return Resultat de la requete sur la base effectu� par le REST avec les infos fournies par le client
     * @throws IOException
     */
    public static String researchTrainsFilter3(String departureStation, String arrivalStation, String departureDate, String arrivalDate, int nbPlaces, String travelClass) throws IOException { // test axis : http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter3?departureStation=NICE&arrivalStation=MARSEILLE&departureDate=2024-01-01&arrivalDate=2024-01-04&nbPlaces=20&travelClass=business
        String urlString = "http://localhost:8182/searching/" + departureStation + "/" + arrivalStation + "/" + departureDate + "/" + arrivalDate + "/" + nbPlaces + "/" + travelClass;
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            return response.toString();
        } else {
            return "GET request not worked, Response Code: " + responseCode;
        }
    }
    
    /** Fonction permetant d'effectuer une r�servation � travers une requete @Post envoy�e au service REST
     * 
     * @param idTrain identifiant du train que le client souhaite r�server
     * @param nbPlaces Nombre total de si�ges que le client reserve
     * @param travelClass cat�gorie de ces si�ges
     * @param flexibilite pr�cise si il s'agit de billets flexible ou non
     * @param userId identifiant du client passant la commande
     * @return "True" si la reservation est prise en compte, "False" si le nombre de si�ge disponible est insufisant
     * @throws IOException
     */
    public static String reservePlaces(String idTrain, int nbPlaces, String travelClass, String flexibilite, String userId) throws IOException { //test axis : http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/reservePlaces?idTrain=1&nbPlaces=2&travelClass=standard&flexibilite=flexible&userId=14
        URL url = new URL("http://localhost:8182/reserve");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        String urlParameters = "trainId=" + idTrain + 
                               "&nbTickets=" + nbPlaces + 
                               "&travelClass=" + travelClass + 
                               "&flexibility=" + flexibilite + 
                               "&userID=" + userId;

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
        writer.write(urlParameters);
        writer.close();

        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            in.close();

            return response.toString();
        } else {
            return "POST request not worked, Response Code: " + responseCode;
        }
    }
    
}

