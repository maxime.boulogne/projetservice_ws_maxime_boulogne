package clientRest;

import java.io.IOException;

import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
//Client afin de tester la methode @POST permetant de r�server des places d'un train, et le service en g�neral
public class ClientRest1 {

	public static void main(String[] args) {
			ClientResource resource = new ClientResource("http://localhost:8182/reserve");  
	 
			Form form = new Form();  
			form.add("trainId", "51");
			form.add("nbTickets", "4");
			form.add("travelClass", "first");
			form.add("flexibility", "flexible");
			form.add("userID", "54");
	 
			try {
				resource.post(form).write(System.out);
			} catch (ResourceException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}  
	}
}
