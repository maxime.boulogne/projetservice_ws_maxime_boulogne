package restONE.main;
 
import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import restONE.bdUtilisation.Train;
import restONE.bdUtilisation.TrainDAO;
 
public class UserFilter1 extends ServerResource {  	
	/**
	 * Methode Get permettant la recherche de train par stations
	 */
	@Get  
	public String toString() {
		//on r�cup�re les stations choisies dans l'url
		String departueStation = (String) getRequestAttributes().get("departureStation");
		String arrivalStation = (String) getRequestAttributes().get("arrivalStation");
		
		StringBuilder sb = new StringBuilder();
		//recherche des train suivant les stations demand�es
		TrainDAO trainDAO = new TrainDAO();
        List<Train> trains = trainDAO.getTrainsByStations(departueStation, arrivalStation);
        
        for (Train train : trains) {
            sb.append(train.toString() + "\n");// Ajoute chaque train � la cha�ne retourn�e
        }

        if(sb.toString().isEmpty()) {
        	return "No available trains.";//si aucun train trouv�s, on informe le client
        }
        return sb.toString();//retour de la chaine de caract�re affichant la liste des trains trouv�
	}
}