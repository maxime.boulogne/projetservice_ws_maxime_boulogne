package restONE.main;
 
import java.sql.SQLException;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import restONE.bdUtilisation.TrainDAO;
 
public class UserUpdate extends ServerResource {  
	/**
	 * M�thode Post permetant de r�server des si�ge d'un train
	 * 
	 * @param entity
	 * @return retourne "True" ou "False" suivant si la r�servation a bien pu �tre r�alis�e ou non
	 */
	@Post
	public Representation acceptItem(Representation entity) {
		Form form = new Form(entity);  
		String trainId = form.getFirstValue("trainId");  
		String nbTickets = form.getFirstValue("nbTickets");  
		String travelClass = form.getFirstValue("travelClass");  
		String flexibility = form.getFirstValue("flexibility");  
		String userID = form.getFirstValue("userID");  
		
		TrainDAO trainDAO = new TrainDAO();
		boolean responseClient;
		try {
			responseClient = trainDAO.addReservationAndUpdateTrain(Integer.valueOf(trainId), Integer.valueOf(nbTickets), travelClass, flexibility, Integer.valueOf(userID));
			if(responseClient){
				System.out.println("True");
				return new StringRepresentation("True", MediaType.TEXT_PLAIN);
			}
			
		} catch (SQLException e) {
			System.out.println("False");
			e.printStackTrace();
		}
		
		return new StringRepresentation("False", MediaType.TEXT_PLAIN);
	}
}  