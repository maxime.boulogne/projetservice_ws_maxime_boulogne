package restONE.main;
 
import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import restONE.bdUtilisation.Train;
import restONE.bdUtilisation.TrainDAO;
 
public class UserFilter3 extends ServerResource {  	
	/**
	 * Methode Get permettant de rechercher les trains suivant des stations et des dates et des si�ges disponibles
	 */
	@Get  
	public String toString() {
		//on r�cup�re les stations choisies / dates choisies / si�ges libres
		String departueStation = (String) getRequestAttributes().get("departureStation");
		String arrivalStation = (String) getRequestAttributes().get("arrivalStation");
		String departureDate = (String) getRequestAttributes().get("departureDate");
		String arrivalDate = (String) getRequestAttributes().get("arrivalDate");
		String nbTickets = (String) getRequestAttributes().get("nbTickets");
		String travelClass = (String) getRequestAttributes().get("travelClass");// ENUM de "business, first" et "Standart" si autre chose
		
		//on remet bien les espaces apr�s le formatage de l'url
		departureDate = departureDate.replace("%20", " ");
		arrivalDate = arrivalDate.replace("%20", " ");
		
		StringBuilder sb = new StringBuilder();
		TrainDAO trainDAO = new TrainDAO();
        List<Train> trains = trainDAO.getTrainsByStationsAndDatesAndTicketsAvaibles(departueStation, arrivalStation, departureDate, arrivalDate, Integer.valueOf(nbTickets), travelClass);
        
        for (Train train : trains) {
            sb.append(train.toString() + "\n");// Ajoute chaque train � la cha�ne retourn�e
        }


        if(sb.toString().isEmpty()) {
        	return "No available trains.";
        }
        return sb.toString();
	}
	
}