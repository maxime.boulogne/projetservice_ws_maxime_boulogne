package restONE.main;
 
import org.restlet.Component;
import org.restlet.data.Protocol;
//Class permettant de lancer le service REST sur le port 8182 en LocalHost
public class RESTDistributor {
 
	public static void main(String[] args) throws Exception {
		Component component = new Component();  	
		component.getServers().add(Protocol.HTTP, 8182);
		
		component.getDefaultHost().attach(new RouterApplication());  
		component.start();  
	}	 
 
}