package restONE.main;
 
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;
//Classe permettant de d�finir les attaches et param�tres des differents liens d'acc�s du service REST
public class RouterApplication extends Application{
	@Override
	public synchronized Restlet createInboundRoot() {
		Router router = new Router(getContext());
		
		//trains filter
		//Redirection des liens de filtrage de train dans les m�thodes @GET associ�es
		router.attach("/searching/{departureStation}/{arrivalStation}", UserFilter1.class); //test : http://localhost:8182/searching/NICE/MARSEILLE
		router.attach("/searching/{departureStation}/{arrivalStation}/{departureDate}/{arrivalDate}", UserFilter2.class); //test : http://localhost:8182/searching/NICE/MARSEILLE/2024-01-01 08:00:00.0/2024-01-04 08:00:00.0
		router.attach("/searching/{departureStation}/{arrivalStation}/{departureDate}/{arrivalDate}/{nbTickets}/{travelClass}", UserFilter3.class); //test : http://localhost:8182/searching/NICE/MARSEILLE/2024-01-01 08:00:00/2024-01-04 08:00:00/3/business
		
		//trains update
		//Redirection du lien vers la m�thode @POST permettant de r�server les places du train
		router.attach("/reserve", UserUpdate.class); //test : form.add("trainId", "19"); form.add("nbTickets", "8"); form.add("travelClass", "business"); form.add("flexibility", "flexible"); form.add("userID", "11");
		return router;
	}
}