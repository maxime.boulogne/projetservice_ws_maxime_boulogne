package restONE.main;
 
import java.util.List;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import restONE.bdUtilisation.Train;
import restONE.bdUtilisation.TrainDAO;
 
public class UserFilter2 extends ServerResource {  	
	/**
	 * Methode Get permettant de rechercher les trains suivant des stations et des dates
	 */
	@Get  
	public String toString() {
		//on r�cup�re les stations choisies / dates choisies
		String departueStation = (String) getRequestAttributes().get("departureStation");
		String arrivalStation = (String) getRequestAttributes().get("arrivalStation");
		String departureDate = (String) getRequestAttributes().get("departureDate");
		String arrivalDate = (String) getRequestAttributes().get("arrivalDate");
		
		//on remet bien les espaces apr�s le formatage de l'url
		departureDate = departureDate.replace("%20", " ");
		arrivalDate = arrivalDate.replace("%20", " ");
		
		System.out.println(departureDate);
		
		StringBuilder sb = new StringBuilder();
		TrainDAO trainDAO = new TrainDAO();
        List<Train> trains = trainDAO.getTrainsByStationsAndDates(departueStation, arrivalStation, departureDate, arrivalDate);
        
        for (Train train : trains) {
            sb.append(train.toString() + "\n");// Ajoute chaque train � la cha�ne retourn�e
        }
        
        if(sb.toString().isEmpty()) {
        	return "No available trains.";
        }
        return sb.toString();
	}	
}