package restONE.bdUtilisation;

import java.sql.Timestamp;
//classe mod�le "Train" qui reprend la forme de la table dans la BD (visible dans le fichier PDF "Information_BDD.pdf")
public class Train {
    private int trainId;
    private String departureStation;
    private String arrivalStation;
    private Timestamp departureDateTime;
    private Timestamp arrivalDateTime;
    private int firstClassSeatsAvailable;
    private int businessClassSeatsAvailable;
    private int standardClassSeatsAvailable;
    private int standardFlexiblePrice;
    private int standardNotFlexiblePrice;
    private int firstFlexiblePrice;
    private int firstNotFlexiblePrice;
    private int businessFlexiblePrice;
    private int businessNotFlexiblePrice;
    
    // Constructeur sans param�tres
    public Train() {
    }

    // Constructeur avec tous les param�tres qui d�finissent un train
    public Train(int trainId, String departureStation, String arrivalStation, Timestamp departureDateTime, Timestamp arrivalDateTime, int firstClassSeatsAvailable, int businessClassSeatsAvailable, int standardClassSeatsAvailable, int standardFlexiblePrice, int standardNotFlexiblePrice, int firstFlexiblePrice, int firstNotFlexiblePrice, int businessFlexiblePrice, int businessNotFlexiblePrice) {
        this.trainId = trainId;
        this.departureStation = departureStation;
        this.arrivalStation = arrivalStation;
        this.departureDateTime = departureDateTime;
        this.arrivalDateTime = arrivalDateTime;
        this.firstClassSeatsAvailable = firstClassSeatsAvailable;
        this.businessClassSeatsAvailable = businessClassSeatsAvailable;
        this.standardClassSeatsAvailable = standardClassSeatsAvailable;
        this.standardFlexiblePrice = standardFlexiblePrice;
        this.standardNotFlexiblePrice = standardNotFlexiblePrice;
        this.firstFlexiblePrice = firstFlexiblePrice;
        this.firstNotFlexiblePrice = firstNotFlexiblePrice;
        this.businessFlexiblePrice = businessFlexiblePrice;
        this.businessNotFlexiblePrice = businessNotFlexiblePrice;
    }

    // Getters et Setters
    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getArrivalStation() {
        return arrivalStation;
    }

    public void setArrivalStation(String arrivalStation) {
        this.arrivalStation = arrivalStation;
    }

    public Timestamp getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Timestamp departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public Timestamp getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Timestamp arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }
    
    public int getFirstClassSeatsAvailable() {
        return firstClassSeatsAvailable;
    }

    public void setFirstClassSeatsAvailable(int firstClassSeatsAvailable) {
        this.firstClassSeatsAvailable = firstClassSeatsAvailable;
    }

    public int getBusinessClassSeatsAvailable() {
        return businessClassSeatsAvailable;
    }

    public void setBusinessClassSeatsAvailable(int businessClassSeatsAvailable) {
        this.businessClassSeatsAvailable = businessClassSeatsAvailable;
    }

    public int getStandardClassSeatsAvailable() {
        return standardClassSeatsAvailable;
    }

    public void setStandardClassSeatsAvailable(int standardClassSeatsAvailable) {
        this.standardClassSeatsAvailable = standardClassSeatsAvailable;
    }
    
    public void setStandardFlexiblePrice(int standardFlexiblePrice) {
        this.standardFlexiblePrice = standardFlexiblePrice;
    }

    public void setStandardNotFlexiblePrice(int standardNotFlexiblePrice) {
        this.standardNotFlexiblePrice = standardNotFlexiblePrice;
    }

    public void setFirstFlexiblePrice(int firstFlexiblePrice) {
        this.firstFlexiblePrice = firstFlexiblePrice;
    }

    public void setFirstNotFlexiblePrice(int firstNotFlexiblePrice) {
        this.firstNotFlexiblePrice = firstNotFlexiblePrice;
    }

    public void setBusinessFlexiblePrice(int businessFlexiblePrice) {
        this.businessFlexiblePrice = businessFlexiblePrice;
    }

    public void setBusinessNotFlexiblePrice(int businessNotFlexiblePrice) {
        this.businessNotFlexiblePrice = businessNotFlexiblePrice;
    }
    
    public int getStandardFlexiblePrice() {
        return standardFlexiblePrice;
    }

    public int getStandardNotFlexiblePrice() {
        return standardNotFlexiblePrice;
    }

    public int getFirstFlexiblePrice() {
        return firstFlexiblePrice;
    }

    public int getFirstNotFlexiblePrice() {
        return firstNotFlexiblePrice;
    }

    public int getBusinessFlexiblePrice() {
        return businessFlexiblePrice;
    }

    public int getBusinessNotFlexiblePrice() {
        return businessNotFlexiblePrice;
    }

    /**Permet d'avoir un affichage visible pour un train
     * 
     * @return revoit une chaine de caract�re regroupant toute les informations d'un train
     */
    @Override
    public String toString() {
        return "Train n'" + trainId + " | Seats avalaibles : " 
        + standardClassSeatsAvailable + " Standard seats : " +  standardFlexiblePrice + " $ F " +  standardNotFlexiblePrice + " $ notF / "
        + firstClassSeatsAvailable + " FirstClass seats : " +  firstFlexiblePrice + " $ F " +  firstNotFlexiblePrice + " $ notF / "
        + businessClassSeatsAvailable + " Business seats : " +  businessFlexiblePrice + " $ F " +  businessNotFlexiblePrice + " $ notF / "
        + "leaves at : \"" + departureDateTime + "\" and arrives at : \"" + arrivalDateTime + "\" |";
    }
}
