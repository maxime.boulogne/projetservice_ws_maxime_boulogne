package restONE.bdUtilisation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import restONE.bdConnection.SQLiteDBConnection;


public class TrainDAO {

	/**Methode de filtrage et de recherche de train suivant une station de d�part et d'arriv�
	 * 
	 * @param depart Station de d�part fournie par l'utilisateur
	 * @param arrival Station d'arriv� fournie par l'utilisateur
	 * @return retourne la liste des trains respectant la station de d�part et d'arriv� dans la table "TRAINS" de la table apr�s requ�te SQL
	 */
    public List<Train> getTrainsByStations(String depart, String arrival) {
        List<Train> trains = new ArrayList<>();
        //Requete SQL ressortant la liste des train respectant les Stations fournies
        String SQL = "SELECT * FROM Trains WHERE DepartureStation = ? AND ArrivalStation = ?";
        //lance la connection � la base
        try (Connection  conn = SQLiteDBConnection.connect();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            
            pstmt.setString(1, depart); // Attache la station de d�part en param�tre, dans la requete
            pstmt.setString(2, arrival); // de m�me pour la station d'arriv�
            ResultSet rs = pstmt.executeQuery(); //Execute la requete une fois construite

            //on explore l'ensemble des ligne retourner dans le resultat de notre requete
            while (rs.next()) {
            	//Pour chaque train, on r�cup�re tout ses attributs afin d'avoir l'objet java au complet
            	Train train = new Train();
            	//ajout de tous les attributs du train 
                train.setTrainId(rs.getInt("TrainId"));
                train.setDepartureStation(rs.getString("DepartureStation"));
                train.setArrivalStation(rs.getString("ArrivalStation"));
                train.setDepartureDateTime(rs.getTimestamp("DepartureDateTime_"));
                train.setArrivalDateTime(rs.getTimestamp("ArrivalDateTime"));
                train.setFirstClassSeatsAvailable(rs.getInt("FirstClassSeatsAvailable"));
                train.setBusinessClassSeatsAvailable(rs.getInt("BusinessClassSeatsAvailable"));
                train.setStandardClassSeatsAvailable(rs.getInt("StandardClassSeatsAvailable"));
                train.setStandardFlexiblePrice(rs.getInt("StandardFlexiblePrice"));
                train.setStandardNotFlexiblePrice(rs.getInt("StandardNotFlexiblePrice"));
                train.setFirstFlexiblePrice(rs.getInt("FirstFlexiblePrice"));
                train.setFirstNotFlexiblePrice(rs.getInt("FirstNotFlexiblePrice"));
                train.setBusinessFlexiblePrice(rs.getInt("BusinessFlexiblePrice"));
                train.setBusinessNotFlexiblePrice(rs.getInt("BusinessNotFlexiblePrice"));
                
                //Puis on ajoute ce train � notre liste
                trains.add(train);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //liste des trains
        return trains;
    }
    
    /**Methode de filtrage et de recherche de train suivant une station de d�part/d'arriv�, une date de d�part/d'arriv�
     * 
     * @param depart Station de d�part choisie
     * @param arrival Station d'arriv� choisie
     * @param dateDepart Date de d�part choisie (la date peut est au forma TimeStamp permettant de selection large allant d'une ann�e � une seconde)
     * @param dateArrive Date d'arriv� choisie
     * @return retourne la liste des trains respectant les indformations chosies
     */
    public List<Train> getTrainsByStationsAndDates(String depart, String arrival, String dateDepart, String dateArrive) {
        List<Train> trains = new ArrayList<>();
        // Ajout des conditions pour les dates dans la requ�te SQL
        String SQL = "SELECT * FROM Trains WHERE DepartureStation = ? AND ArrivalStation = ? AND DepartureDateTime_ >= ? AND ArrivalDateTime <= ?";

        try (Connection conn = SQLiteDBConnection.connect();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            
            pstmt.setString(1, depart);
            pstmt.setString(2, arrival);
            pstmt.setString(3, dateDepart); // D�finir le Timestamp pour le d�part
            pstmt.setString(4, dateArrive); // et pour l'arriv�e
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Train train = new Train();
                
                train.setTrainId(rs.getInt("TrainId"));
                train.setDepartureStation(rs.getString("DepartureStation"));
                train.setArrivalStation(rs.getString("ArrivalStation"));
                train.setDepartureDateTime(rs.getTimestamp("DepartureDateTime_"));
                train.setArrivalDateTime(rs.getTimestamp("ArrivalDateTime"));
                train.setFirstClassSeatsAvailable(rs.getInt("FirstClassSeatsAvailable"));
                train.setBusinessClassSeatsAvailable(rs.getInt("BusinessClassSeatsAvailable"));
                train.setStandardClassSeatsAvailable(rs.getInt("StandardClassSeatsAvailable"));
                train.setStandardFlexiblePrice(rs.getInt("StandardFlexiblePrice"));
                train.setStandardNotFlexiblePrice(rs.getInt("StandardNotFlexiblePrice"));
                train.setFirstFlexiblePrice(rs.getInt("FirstFlexiblePrice"));
                train.setFirstNotFlexiblePrice(rs.getInt("FirstNotFlexiblePrice"));
                train.setBusinessFlexiblePrice(rs.getInt("BusinessFlexiblePrice"));
                train.setBusinessNotFlexiblePrice(rs.getInt("BusinessNotFlexiblePrice"));
                
                trains.add(train);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return trains;
    }
    
    /**Methode de filtrage et de recherche de train suivant une station de d�part/d'arriv�, une date de d�part/d'arriv�, un nombre de tockets pour une classe de siege disponibles
     * 
     * @param depart Station de d�part choisie
     * @param arrival Station d'arriv� choisie
     * @param dateDepart Date de d�part choisie (la date peut est au forma TimeStamp permettant de selection large allant d'une ann�e � une seconde)
     * @param dateArrive Date d'arriv� choisie
     * @param nbTickets Nombre de place choisies
     * @param travelClass Class de voyage choisie
     * @return retourne la liste des trains respectant les indformations chosies
     */
    public List<Train> getTrainsByStationsAndDatesAndTicketsAvaibles(String depart, String arrival, String dateDepart, String dateArrive, int nbTickets, String travelClass) {
        List<Train> trains = new ArrayList<>();
        // Construction dynamique de la requ�te SQL pour la disponibilit� des si�ges dans la classe choisie
        String seatAvailabilityColumn;
        switch (travelClass.toLowerCase()) {
            case "business":
                seatAvailabilityColumn = "BusinessClassSeatsAvailable";
                break;
            case "first":
                seatAvailabilityColumn = "FirstClassSeatsAvailable";
                break;
            default:
                seatAvailabilityColumn = "StandardClassSeatsAvailable";
        }

        String SQL = "SELECT * FROM Trains WHERE DepartureStation = ? AND ArrivalStation = ? AND DepartureDateTime_ >= ? AND ArrivalDateTime <= ? AND " + seatAvailabilityColumn + " >= ?";

        try (Connection conn = SQLiteDBConnection.connect();
             PreparedStatement pstmt = conn.prepareStatement(SQL)) {
            
            pstmt.setString(1, depart);
            pstmt.setString(2, arrival);
            pstmt.setString(3, dateDepart);
            pstmt.setString(4, dateArrive);
            pstmt.setInt(5, nbTickets); // D�finir le nombre de billets
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Train train = new Train();

                train.setTrainId(rs.getInt("TrainId"));
                train.setDepartureStation(rs.getString("DepartureStation"));
                train.setArrivalStation(rs.getString("ArrivalStation"));
                train.setDepartureDateTime(rs.getTimestamp("DepartureDateTime_"));
                train.setArrivalDateTime(rs.getTimestamp("ArrivalDateTime"));
                train.setFirstClassSeatsAvailable(rs.getInt("FirstClassSeatsAvailable"));
                train.setBusinessClassSeatsAvailable(rs.getInt("BusinessClassSeatsAvailable"));
                train.setStandardClassSeatsAvailable(rs.getInt("StandardClassSeatsAvailable"));
                train.setStandardFlexiblePrice(rs.getInt("StandardFlexiblePrice"));
                train.setStandardNotFlexiblePrice(rs.getInt("StandardNotFlexiblePrice"));
                train.setFirstFlexiblePrice(rs.getInt("FirstFlexiblePrice"));
                train.setFirstNotFlexiblePrice(rs.getInt("FirstNotFlexiblePrice"));
                train.setBusinessFlexiblePrice(rs.getInt("BusinessFlexiblePrice"));
                train.setBusinessNotFlexiblePrice(rs.getInt("BusinessNotFlexiblePrice"));

                trains.add(train);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return trains;
    }
    
    /** Permet � l'utilisateur de r�server un ou plusieurs si�ges d'un class d'un train, en cr�ant une reservation dans la table et en soustraillant les places selectionn�es au train 
     * 
     * @param trainID Identifiant du train selectionn�
     * @param nbTickets Nombre de places r�serv�
     * @param travelClass Class de si�ge choisie ("business","first","normal")
     * @param flexibility Choix de billets flexibles ou non
     * @param userID identifiant du client
     * @return Renvoie "true" si la reservation c'est bien pass�e et "false" si elle n'a pas pu se faire par cause de place insufisante/informations �rron�es
     * @throws SQLException Dans le cas ou la reservation rencontre un probl�me
     */
    public boolean addReservationAndUpdateTrain(int trainID, int nbTickets, String travelClass, String flexibility, int userID) throws SQLException {
        Connection conn = SQLiteDBConnection.connect();

        // V�rifier si les si�ges demand�s par l'utilisateur sont disponibles, sinon la reservation ne peut pas se faire
        String seatColumn;
        switch (travelClass.toLowerCase()) {
        case "business":
        	seatColumn = "BusinessClassSeatsAvailable";
        	break;
        case "first":
        	seatColumn = "FirstClassSeatsAvailable";
        	break;
        default:
        	seatColumn = "StandardClassSeatsAvailable";
        }
        //requete afin de verifier la disponibilit� des places
        String sqlCheckSeats = "SELECT " + seatColumn + " FROM Trains WHERE TrainId = ?";
        PreparedStatement pstmtCheckSeats = conn.prepareStatement(sqlCheckSeats);
        pstmtCheckSeats.setInt(1, trainID);
        ResultSet rsSeats = pstmtCheckSeats.executeQuery();
        if (rsSeats.next()) {
            int availableSeats = rsSeats.getInt(seatColumn);
            if (availableSeats < nbTickets) {
                rsSeats.close();
                pstmtCheckSeats.close();
                conn.close();
                return false; // Pas assez de si�ges disponibles
            }
        }
        //si assez de si�ges on lance la reservation :
        
        // Trouver le prochain ID de r�servation disponible afin d'�viter les doublons dans la table "RESERVATION"
        String sqlMaxId = "SELECT MAX(ReservationID) + 1 AS nextId FROM Reservations";
        Statement stmtMaxId = conn.createStatement();
        ResultSet rs = stmtMaxId.executeQuery(sqlMaxId);
        rs.next();
        int nextReservationID = rs.getInt("nextId");

        // Insertion la nouvelle r�servation avec les information fournies
        String sqlInsert = "INSERT INTO Reservations (ReservationID, Class, TicketType, NumberOfTickets, Status, TrainId, UserID) VALUES (?, ?, ?, ?, 'Active', ?, ?)";
        PreparedStatement pstmtReservation = conn.prepareStatement(sqlInsert);
        pstmtReservation.setInt(1, nextReservationID);
        pstmtReservation.setString(2, travelClass);
        pstmtReservation.setString(3, flexibility);
        pstmtReservation.setInt(4, nbTickets);
        pstmtReservation.setInt(5, trainID);
        pstmtReservation.setInt(6, userID);
        pstmtReservation.executeUpdate();

        // Mettre � jour les si�ges disponibles du train en cons�quence
        String sqlUpdate = "UPDATE Trains SET " + seatColumn + " = " + seatColumn + " - ? WHERE TrainId = ?";
        PreparedStatement pstmtUpdateTrain = conn.prepareStatement(sqlUpdate);
        pstmtUpdateTrain.setInt(1, nbTickets);
        pstmtUpdateTrain.setInt(2, trainID);
        pstmtUpdateTrain.executeUpdate();

        // Fermeture des ressources
        rs.close();
        stmtMaxId.close();
        pstmtReservation.close();
        pstmtUpdateTrain.close();
        pstmtCheckSeats.close();
        conn.close();

        return true; // R�servation r�ussie
    }

}