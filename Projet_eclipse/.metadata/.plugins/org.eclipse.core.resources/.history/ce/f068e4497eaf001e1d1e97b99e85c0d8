package client.main;

import java.nio.charset.StandardCharsets;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class ClientSoap {
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filter1(String departureStation, String arrivalStation) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter1";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s", baseUrl, departureStation, arrivalStation);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s pour researchTrainsFilter2
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @param departureDate La date de d�part
     * @param arrivalDate La date d'arriv�e
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filter2(String departureStation, String arrivalStation, String departureDate, String arrivalDate) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter2";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s&departureDate=%s&arrivalDate=%s", baseUrl, departureStation, arrivalStation, departureDate, arrivalDate);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s pour researchTrainsFilter3
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @param departureDate La date de d�part
     * @param arrivalDate La date d'arriv�e
     * @param nbPlaces Le nombre de places
     * @param travelClass La classe de voyage
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filtre3(String departureStation, String arrivalStation, String departureDate, String arrivalDate, int nbPlaces, String travelClass) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter3";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s&departureDate=%s&arrivalDate=%s&nbPlaces=%d&travelClass=%s", baseUrl, departureStation, arrivalStation, departureDate, arrivalDate, nbPlaces, travelClass);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP POST au service de train pour r�server des places
     *
     * @param idTrain L'identifiant du train
     * @param nbPlaces Le nombre de places � r�server
     * @param travelClass La classe de voyage (standard, business, first)
     * @param flexibilite La flexibilit� du billet (flexible, non-flexible).
     * @param userId L'identifiant de l'utilisateur.
     * @return La r�ponse du service web sous forme de cha�ne de caract�res.
     */
    public static String reserveTrainPlaces(String idTrain, int nbPlaces, String travelClass, String flexibilite, String userId) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/reservePlaces";
        String url = baseUrl;

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost postRequest = new HttpPost(url);

            // Construction du corps de la requ�te
            String jsonBody = String.format("{\"idTrain\":%s,\"nbPlaces\":%d,\"travelClass\":\"%s\",\"flexibilite\":\"%s\",\"userId\":%s}", idTrain, nbPlaces, travelClass, flexibilite, userId);
            StringEntity entity = new StringEntity(jsonBody, StandardCharsets.UTF_8);
            postRequest.setEntity(entity);
            postRequest.setHeader("Content-type", "application/json");

            // Envoi de la requ�te et r�cup�ration de la r�ponse
            return httpClient.execute(postRequest, httpResponse ->
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) {
        // Exemple d'utilisation filtre1
        String result1 = filter1("NICE", "PARIS");
        System.out.println("filtre1: \n" + result1);
        //filtre 2
        String result2 = filter2("NICE", "MARSEILLE", "2024-01-01", "2024-01-04");
        System.out.println("\n filtre2: \n" + result2);
        //filtre 3
        String result3 = filtre3("NICE", "MARSEILLE", "2024-01-01", "2024-01-04", 20, "business");
        System.out.println("\n filtre3: \n" + result3);
        //filtre 4
        String result4 = reserveTrainPlaces("2", 2, "standard", "flexible", "14");
        System.out.println("\n reservation: \n" + result4);
    }
}

