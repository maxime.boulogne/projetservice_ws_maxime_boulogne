package client.main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class ClientSoap {
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filter1(String departureStation, String arrivalStation) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter1";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s", baseUrl, departureStation, arrivalStation);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s pour researchTrainsFilter2
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @param departureDate La date de d�part
     * @param arrivalDate La date d'arriv�e
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filter2(String departureStation, String arrivalStation, String departureDate, String arrivalDate) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter2";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s&departureDate=%s&arrivalDate=%s", baseUrl, departureStation, arrivalStation, departureDate, arrivalDate);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP GET au service de train avec des param�tres sp�cifi�s pour researchTrainsFilter3
     *
     * @param departureStation La gare de d�part
     * @param arrivalStation La gare d'arriv�e
     * @param departureDate La date de d�part
     * @param arrivalDate La date d'arriv�e
     * @param nbPlaces Le nombre de places
     * @param travelClass La classe de voyage
     * @return La r�ponse du service web sous forme de cha�ne de caract�res
     */
    public static String filtre3(String departureStation, String arrivalStation, String departureDate, String arrivalDate, int nbPlaces, String travelClass) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/researchTrainsFilter3";
        String url = String.format("%s?departureStation=%s&arrivalStation=%s&departureDate=%s&arrivalDate=%s&nbPlaces=%d&travelClass=%s", baseUrl, departureStation, arrivalStation, departureDate, arrivalDate, nbPlaces, travelClass);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Envoie une requ�te HTTP POST au service de train pour r�server des places
     *
     * @param idTrain L'identifiant du train
     * @param nbPlaces Le nombre de places � r�server
     * @param travelClass La classe de voyage (standard, business, first)
     * @param flexibilite La flexibilit� du billet (flexible, non-flexible).
     * @param userId L'identifiant de l'utilisateur.
     * @return La r�ponse du service web sous forme de cha�ne de caract�res.
     */
    
    /**
     * Envoie une requ�te HTTP GET (mais POST depuis le SOAP vers REST) pour r�server des places sur un train.
     *
     * @param idTrain L'identifiant du train.
     * @param nbPlaces Le nombre de places � r�server.
     * @param travelClass La classe de voyage (par exemple, standard, premi�re classe, etc.).
     * @param flexibilite Le type de flexibilit� (par exemple, flexible, non-flexible).
     * @param userId L'identifiant de l'utilisateur qui effectue la r�servation.
     * @return La r�ponse du service web sous forme de cha�ne de caract�res.
     */
    public static String reserveTrainPlaces(int idTrain, int nbPlaces, String travelClass, String flexibilite, int userId) {
        String baseUrl = "http://localhost:8080/TrainServiceSOAP/services/PaserelleSOAP/reservePlaces";
        String url = String.format("%s?idTrain=%d&nbPlaces=%d&travelClass=%s&flexibilite=%s&userId=%d", baseUrl, idTrain, nbPlaces, travelClass, flexibilite, userId);

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            return httpClient.execute(request, httpResponse -> 
                EntityUtils.toString(httpResponse.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /** Permet de caster le resultat xml re�u en resortant uniquement le contenu
     * 
     * @param xmlResponse Resultat xml � caster avant affichage
     * @return retourne le resultat du xml, filtrer sans les balise
     */
    private static String extractContent(String xmlResponse) {
        Pattern pattern = Pattern.compile("<ns:return>(.*?)</ns:return>", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(xmlResponse);
        if (matcher.find()) {
            return matcher.group(1).trim();
        }
        return "Aucun contenu trouv�";
    }

    public static void main(String[] args) {
        // Exemple d'utilisation filtre1
        String result1 = filter1("NICE", "PARIS");
        System.out.println("filtre1: \n" + extractContent(result1));
        //filtre 2
        String result2 = filter2("NICE", "MARSEILLE", "2024-01-01", "2024-01-04");
        System.out.println("\nfiltre2: \n" + extractContent(result2));
        //filtre 3
        String result3 = filtre3("NICE", "MARSEILLE", "2024-01-01", "2024-01-04", 20, "business");
        System.out.println("\nfiltre3: \n" + extractContent(result3));
        //filtre 4
        String result4 = reserveTrainPlaces(22, 4, "business", "flexible", 87);
        System.out.println("\nreservation: \n" + extractContent(result4));
    }
}

